function logout() {
    $.ajax({
        type: "POST",
        url: "http://active-tangent-159416.appspot.com/rest/logout/" + localStorage.getItem('username'),
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        //dataType: "json",
        success: function(response) {
            if(response) {
                localStorage.removeItem('tokenID');
                localStorage.removeItem('username');
                window.location.href = "http://active-tangent-159416.appspot.com/login.html";
            }
            else {
                alert("No response");
            }
        },
        error: function(response) {
            alert("Error: "+ response.status);
        },
        data: JSON.stringify(JSON.parse("{\"tokenID\":\"" + localStorage.getItem('tokenID') + "\"}"))
    });

    event.preventDefault();
};

/*window.onload = function() {
    var frms = $('form[name="logout"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
}*/
