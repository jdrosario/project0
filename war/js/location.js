
var map;


function initMap() 
{
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat:  38.659784, lng:  -9.202765},
		zoom: 16
	});
}

function geocodeAddress(geocoder, resultsMap, address) {

	geocoder.geocode({'address': address}, function(results, status) {
		if (status === 'OK') {
			resultsMap.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: resultsMap,
				position: results[0].geometry.location
			});
		} else {
			alert('Geocode was not successful for the following reason: ' + status);
		}
	});
};

function getLoc() {
	$.ajax({
		type: "POST",
		url: "http://active-tangent-159416.appspot.com/rest/operation/getAddress/" + localStorage.getItem('username'),
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			if(response) {
				initMap();
				var geocoder = new google.maps.Geocoder();
				geocodeAddress(geocoder, map, response);
			}
			else {
				alert("No response");
				window.location.href = "http://active-tangent-159416.appspot.com/index.html";
			}
		},
		error: function(response) {
			alert("Error: "+ response.status);
			window.location.href = "http://active-tangent-159416.appspot.com/login.html";
		},
		data: JSON.stringify(JSON.parse("{\"tokenID\":\"" + localStorage.getItem('tokenID') + "\"}"))
	});

}; 


